import axios from "axios";

axios.defaults.baseURL = 'https://newsapi.org/v2/';

const mxnNews = {

    getNews: (params) => {
        return axios.get('/top-headlines',{
            params: params
        });
    },
    getNewsById: (params) => {
        return axios.post('/login',params);
    },

    
}

export default mxnNews;