import Item from "../item/Item";
import mxnNews from '@/mixins/news';
import {countries} from '@/utils/countries'
import { mapGetters } from 'vuex'

export default {
    name: 'Home',
    mixins: ['mxnNews'],
    computed: {
        ...mapGetters(['articleList']),
        countrySelection() {
            return countries;
        },
        categorySelection() {
            return ["business", "entertainment", "general", "health", "science", "sports", "technology",]
        }
    },
    data () {
        return {
            country: 'ph',
            category: 'technology',
            search: '',
            currentPage: 1,
            totalResult: 0,
            isLoading: false,
            scTimer: 0,
            scY: 0,
        }
    },

    created: function () {
        
    },

    methods: {
        getTopNews() {
            const _this = this;
            const p = {
                country: _this.country,
                category: _this.category,
                // pageSize: 30,
                page: _this.currentPage,
                apiKey: 'e22bf9af940b4d70b2cc44b043dea570'
            }

            if (_this.search) {
                p.q = _this.search;
            }
            // start loader
            _this.isLoading = true;
            mxnNews.getNews(p).then(response => {
                const data = response.data;
                
                const articles = [
                    ..._this.articleList,
                    ...data.articles
                ]
                _this.totalResult = data.totalResults
                _this.$store.dispatch("doStoreArticle", articles)

                // end of loader
                _this.isLoading = false;
            }, 
            error => {

            })
        },

        searchNews() {
            const _this = this;
            _this.$store.dispatch("resetArticle")
            _this.currentPage = 1,
            _this.totalResult = 0,
            _this.getTopNews();
        },

        refresh() {
            const _this = this;
            _this.search = '';
            _this.searchNews()
        },
        showMore() {
            const _this = this;

            if (!_this.isLoading) {
                _this.currentPage++;
                _this.getTopNews();
            }
            // console.info(_this.articleList)
        },
        handleScroll: function (event) {
            // console.info(event)
            const _this = this;
            const {scrollHeight, scrollTop, clientHeight} = event.target;
            // console.info(Math.abs(scrollHeight - clientHeight - scrollTop), '<' + 1)
            var D = document;
            const a = Math.max(
                D.body.scrollHeight, D.documentElement.scrollHeight,
                D.body.offsetHeight, D.documentElement.offsetHeight,
                D.body.clientHeight, D.documentElement.clientHeight
            );

            // console.info(a - 1, window.visualViewport.height + window.scrollY)
            if (((a - 1) <= window.visualViewport.height + window.scrollY) &&  _this.totalResult > _this.articleList.length) {
                _this.showMore();
            }

            if (_this.scTimer) return;
            _this.scTimer = setTimeout(() => {
              _this.scY = window.scrollY;
              clearTimeout(_this.scTimer);
              _this.scTimer = 0;
            }, 100);
          },
        toTop() {
            window.scrollTo({
              top: 0,
              behavior: "smooth"
            });
        },
    },

    
    mounted () {
        this.getTopNews();

        window.addEventListener('scroll', this.handleScroll);
    },

    components: {
        Item
    }
}