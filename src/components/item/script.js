import router from "@/router";


export default {
    name: 'Item',
    props: ['article'],
    mixins: [],
    computed: {
    },
    data () {
        return {
            showModal: false
        }
    },

    created: function () {
        
    },

    methods: {
        getPublishedDate() {
            const _this = this;
            const publishedDate = _this.article.publishedAt;
            const dateOptions = {
                day: 'numeric',
                month: 'long',
                year: 'numeric'
              };
            const date = new Date(publishedDate)
            return date.toLocaleString('en-US', dateOptions)
        },
        visitSite(url) {
            window.open( url, '_blank' );
        },
        showNewsDetails() {
            // router.push('/news')
            const _this = this;
            _this.showModal = true;
            
        }
    },
    
    mounted () {
        const _this = this;
        // console.info(_this.article)
    },

    components: {
        
    }
}