import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    articleList: []
  },
  getters: {
    articleList : state => {
			return state.articleList
	}
  },
  mutations: {
    storeArticle (state, info) {
      state.articleList = info
    }
  },
  actions: {
    doStoreArticle({commit}, data) {
      commit('storeArticle', data)
    },
    resetArticle({commit}) {
      commit('storeArticle', [])
    },

  },
  modules: {
  }
})
